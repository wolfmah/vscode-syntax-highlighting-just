# Change Log

All notable changes to the `just-syntax` extension will be documented in this file.

## [Unreleased]
### Added
### Changed
### Removed
### Fix

## [0.1.0] - 2024-02-20
### Added
- Initial release
