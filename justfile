# use with https://github.com/casey/just

set dotenv-load

PACKAGE_BASENAME := "out/just-syntax-"
CURRENT_VERSION := `cat package.json | jq '.version' | cut -d '"' -f 2`
CURRENT_TAG := `git describe --tags --abbrev=0 2> /dev/null || true`
TAG_EXIST := if CURRENT_VERSION == CURRENT_TAG { "true" } else { "false" }

_default:
  @just --list --justfile "{{ justfile() }}"

build:
  @npm run compile

# [<newversion> | major | minor | patch | premajor | preminor | prepatch | prerelease | from-git]
version new_version:
  @npm version {{ new_version }} --no-git-tag-version

tag:
  #!/usr/bin/env bash
  set -euo pipefail

  if [[ "{{ TAG_EXIST }}" != "true" ]]; then
    git tag -a {{ CURRENT_VERSION }}
    git push --tags
  fi

package:
  @mkdir -p out
  @npx @vscode/vsce package
  @mv *.vsix out/

publish-vsmarket:
  @npx @vscode/vsce publish --packagePath {{ PACKAGE_BASENAME }}{{ CURRENT_VERSION }}.vsix

publish-ovsx:
  @npx ovsx publish --packagePath {{ PACKAGE_BASENAME }}{{ CURRENT_VERSION }}.vsix
